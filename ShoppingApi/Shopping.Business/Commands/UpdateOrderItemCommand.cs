﻿namespace Shopping.Business.Commands
{
    public class UpdateOrderItemCommand
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
