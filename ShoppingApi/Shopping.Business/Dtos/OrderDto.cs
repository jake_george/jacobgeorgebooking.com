﻿using System.Collections.Generic;

namespace Shopping.Business.Dtos
{
    public class OrderDto
    {
        public int OrderId { get; set; }
        public List<OrderItemDto> Items { get; set; }
    }
}
