﻿using System.Collections.Generic;

namespace Shopping.Business.Dtos
{
    public class ResultDto<TResult> where TResult : class
    {
        public TResult Result { get; set; }
        public List<string> Errors { get; set; }
        public bool HasErrors { get { return Errors.Count > 0; } }

        public ResultDto()
        {
            Errors = new List<string>();
        }
    }
}
