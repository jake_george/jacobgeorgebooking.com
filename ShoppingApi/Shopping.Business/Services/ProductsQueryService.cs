﻿using System.Linq;
using System.Collections.Generic;

using Shopping.Business.Dtos;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Services
{
    public interface IProductsQueryService
    {
        ResultDto<List<ProductDto>> GetInStockProducts();
    }

    public class ProductsQueryService : IProductsQueryService
    {
        private IProductRepository _productsRepository;
        public ProductsQueryService(IProductRepository productsRepository)
        {
            _productsRepository = productsRepository;
        }

        public ResultDto<List<ProductDto>> GetInStockProducts()
        {
            var result = new ResultDto<List<ProductDto>>();

            var allProducts = _productsRepository.GetAllInStockProducts();

            result.Result = allProducts.Select(c => new ProductDto()
            {
                ProductId = c.Id,
                Name = c.Name,
                Description = c.Description
            }).ToList();

            return result;
        }    
    }
}
