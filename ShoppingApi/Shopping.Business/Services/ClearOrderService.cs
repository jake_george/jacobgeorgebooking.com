﻿using System.Linq;

using Shopping.Business.Dtos;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Services
{
    public interface IClearOrderService
    {
        ResultDto<OrderDto> ClearItems(int orderId);
    }

    public class ClearOrderService : IClearOrderService
    {
        private IUnitOfWork _unitOfWork;
        private IOrderRepository _orderRepository;
        public ClearOrderService(IUnitOfWork unitOfWork,
                                  IOrderRepository orderRepository)
        {
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
        }

        public ResultDto<OrderDto> ClearItems(int orderId)
        {
            var result = new ResultDto<OrderDto>();

            var order = _orderRepository.GetById(orderId);
            if (order == null)
            {
                result.Errors.Add($"Order with id {orderId} does not exist");
            }
            else
            {
                order.OrderProducts.Clear();

                _unitOfWork.SaveChanges();

                result.Result = new OrderDto();
                result.Result.OrderId = order.Id;
                result.Result.Items = order.OrderProducts.Select(c => new OrderItemDto()
                {
                    ProductId = c.Product.Id,
                    Name = c.Product.Name,
                    Description = c.Product.Description,
                    Quantity = c.Quantity
                }).ToList();
            }

            return result;
        }
    }
}
