﻿using System;
using System.Collections.Generic;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Services
{
    public interface ICreateOrderService
    {
        ResultDto<OrderDto> CreateOrder();
    }

    public class CreateOrderService : ICreateOrderService
    {
        private IUnitOfWork _unitOfWork;
        private IOrderRepository _orderRepository;
        public CreateOrderService(IUnitOfWork unitOfWork,
                                  IOrderRepository orderRepository)
        {
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
        }

        public ResultDto<OrderDto> CreateOrder()
        {
            var result = new ResultDto<OrderDto>();

            var newOrder = new Order();
            newOrder.DateCreated = DateTime.Now;

            var savedOrder = _orderRepository.Add(newOrder);

            this._unitOfWork.SaveChanges();

            result.Result = new OrderDto();
            result.Result.OrderId = savedOrder.Id;
            result.Result.Items = new List<OrderItemDto>();

            return result;
        }
    }
}
