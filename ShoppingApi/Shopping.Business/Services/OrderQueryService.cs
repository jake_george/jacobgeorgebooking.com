﻿using System.Linq;

using Shopping.Business.Dtos;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Services
{
    public interface IOrderQueryService
    {
        ResultDto<OrderDto> GetOrder(int id);
    }

    public class OrderQueryService : IOrderQueryService
    {
        private IOrderRepository _orderRepository;
        public OrderQueryService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public ResultDto<OrderDto> GetOrder(int id)
        {
            var result = new ResultDto<OrderDto>();

            var existingOrder = _orderRepository.GetById(id);
            if (existingOrder == null)
            {
                result.Errors.Add($"Order with id {id} does not exist");
            }
            else
            {
                result.Result = new OrderDto();
                result.Result.OrderId = existingOrder.Id;
                result.Result.Items = existingOrder.OrderProducts.Select(c => new OrderItemDto()
                {                    
                    ProductId = c.Product.Id,
                    Name = c.Product.Name,
                    Description = c.Product.Description,
                    Quantity = c.Quantity                    
                }).ToList();
            }

            return result;
        }    
    }
}
