﻿using System;
using System.Linq;
using System.Data;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Commands;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Services
{
    public interface IUpdateOrderItemService
    {
        ResultDto<OrderDto> UpdateItemQuantity(UpdateOrderItemCommand updateOrder);
    }

    public class UpdateOrderItemService : IUpdateOrderItemService
    {
        private IUnitOfWork _unitOfWork;
        private IOrderRepository _orderRepository;
        private IProductRepository _productRepository;
        public UpdateOrderItemService(IUnitOfWork unitOfWork,
                                  IOrderRepository orderRepository,
                                  IProductRepository productRepository)
        {
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
            _productRepository = productRepository;
        }

        public ResultDto<OrderDto> UpdateItemQuantity(UpdateOrderItemCommand updateOrder)
        {           
            var result = new ResultDto<OrderDto>();
            using (var transaction = _unitOfWork.BeginTransaction(IsolationLevel.Serializable)) // should probably reconsider for performance issues
            {
                var existingOrder = _orderRepository.GetById(updateOrder.OrderId);
                if (existingOrder == null)
                {
                    result.Errors.Add($"Order with id {updateOrder.OrderId} does not exist");
                }
                else
                {
                    var productToAdd = _productRepository.GetById(updateOrder.ProductId);
                    if (productToAdd == null)
                    {
                        result.Errors.Add($"Product with id {updateOrder.ProductId} does not exist");
                    }
                    else
                    {
                        if (updateOrder.Quantity > productToAdd.Stock)
                        {
                            result.Errors.Add($"We currently do not have the required stock ({updateOrder.Quantity}) in place for {productToAdd.Name}");
                        }
                        else
                        {
                            try
                            {
                                var orderProduct = existingOrder.OrderProducts.SingleOrDefault(c => c.ProductId == productToAdd.Id);
                                if (orderProduct == null)
                                {
                                    orderProduct = new OrderProduct();
                                    orderProduct.OrderId = existingOrder.Id;
                                    orderProduct.ProductId = productToAdd.Id;
                                    existingOrder.OrderProducts.Add(orderProduct);
                                }

                                orderProduct.Quantity = updateOrder.Quantity;

                                _unitOfWork.SaveChanges();

                                transaction.Commit();

                                result.Result = new OrderDto();
                                result.Result.OrderId = existingOrder.Id;
                                result.Result.Items = existingOrder.OrderProducts.Select(s => new OrderItemDto()
                                {
                                    Name = s.Product.Name,
                                    Description = s.Product.Description,
                                    Quantity = s.Quantity,
                                    ProductId = s.ProductId
                                }).ToList();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                            }
                        }
                    }
                }
            }

            return result;
        }        
    }
}
