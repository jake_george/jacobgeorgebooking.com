﻿using System;
using System.Linq;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class CreateOrderService_when_create_called
    {
        private Mock<IUnitOfWork> _currentUnitOfWork;
        private Mock<IOrderRepository> _orderRepository;
        private ICreateOrderService _createOrderService;
        private ResultDto<OrderDto> result = null;

        [TestInitialize]
        public void Initialise()
        {
            _currentUnitOfWork = new Mock<IUnitOfWork>();
            _orderRepository = new Mock<IOrderRepository>();
            _createOrderService = new CreateOrderService(_currentUnitOfWork.Object, _orderRepository.Object);

            _orderRepository.Setup(c => c.Add(It.IsAny<Order>())).Returns(new Order() { Id = 2, DateCreated = DateTime.Now });

            result = _createOrderService.CreateOrder();
        }

        [TestMethod]
        public void sets_date_created()
        {
            _orderRepository.Verify(a => a.Add(It.Is<Order>(c => c.DateCreated.ToShortDateString() == DateTime.Now.ToShortDateString())), Times.Once);
        }

        [TestMethod]
        public void adds_new_order_to_repository()
        {
            _orderRepository.Verify(a => a.Add(It.Is<Order>(c => c.DateCreated.ToShortDateString() == DateTime.Now.ToShortDateString())), Times.Once);
        }

        [TestMethod]
        public void saves_unit_of_work()
        {
            _currentUnitOfWork.Verify(c => c.SaveChanges(), Times.Once);
        }

        [TestMethod]
        public void does_not_return_error()
        {
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
        }

        [TestMethod]
        public void returns_saved_order()
        {
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsNotNull(result.Result.Items);
            Assert.IsTrue(result.Result.Items.ToList().Count == 0);
        }
    }
}
