﻿using System;
using System.Linq;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class ClearOrderService_when_clear_called_with_valid_args
    {         
        private IClearOrderService _clearOrderService;
        private Mock<IOrderRepository> _orderRepository;
        private Mock<IUnitOfWork> _currentUnitOfWork;
        private ResultDto<OrderDto> result = null;

        private int _orderId = 2;
        private Order _orderToReturn = null;

        [TestInitialize]
        public void Initialise()
        {
            _orderRepository = new Mock<IOrderRepository>();
            _currentUnitOfWork = new Mock<IUnitOfWork>();
            _clearOrderService = new ClearOrderService(_currentUnitOfWork.Object,
                                                       _orderRepository.Object);
            _orderToReturn = new Order()
            {
                Id = _orderId,
                DateCreated = DateTime.Now,
                OrderProducts = new List<OrderProduct>()
                {
                    new OrderProduct() { OrderId = _orderId, ProductId = 11 },
                    new OrderProduct() { OrderId = _orderId, ProductId = 12 },
                    new OrderProduct() { OrderId = _orderId, ProductId = 13 },
                }
            };

            _orderRepository.Setup(c => c.GetById(It.Is<int>(v => v == _orderToReturn.Id))).Returns(_orderToReturn);

            result = _clearOrderService.ClearItems(_orderId);
        }
        

        [TestMethod]
        public void calls_repository_to_get_order()
        {
            _orderRepository.Verify(c => c.GetById(It.Is<int>(v => v == _orderToReturn.Id)), Times.Once);
        }     

        [TestMethod]
        public void saves_the_unit_of_work()
        {
            _currentUnitOfWork.Verify(c => c.SaveChanges(), Times.Once);
        }      

        [TestMethod]
        public void does_not_return_error()
        {
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
        }

        [TestMethod]
        public void returns_saved_order()
        {
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsTrue(result.Result.OrderId == _orderToReturn.Id);
            Assert.IsTrue(result.Result.Items.ToList().Count == 0);
        }
    }
}
