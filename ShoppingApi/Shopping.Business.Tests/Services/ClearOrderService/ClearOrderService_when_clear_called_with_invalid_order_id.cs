﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class ClearOrderService_when_clear_called_with_invalid_order_id
    {         
        private IClearOrderService _clearOrderService;
        private Mock<IOrderRepository> _orderRepository;
        private Mock<IUnitOfWork> _currentUnitOfWork;
        private ResultDto<OrderDto> result = null;
        private int _orderId = 2;

        [TestInitialize]
        public void Initialise()
        {
            _orderRepository = new Mock<IOrderRepository>();
            _currentUnitOfWork = new Mock<IUnitOfWork>();
            _clearOrderService = new ClearOrderService(_currentUnitOfWork.Object,
                                                       _orderRepository.Object);

            result = _clearOrderService.ClearItems(_orderId);
        }
        

        [TestMethod]
        public void calls_repository_to_get_order()
        {
            _orderRepository.Verify(c => c.GetById(It.Is<int>(v => v == _orderId)), Times.Once);
        }     

        [TestMethod]
        public void does_not_save_the_unit_of_work()
        {
            _currentUnitOfWork.Verify(c => c.SaveChanges(), Times.Never);
        }

        [TestMethod]
        public void returns_error()
        {
            Assert.IsTrue(result.HasErrors);
            Assert.IsTrue(result.Errors.Count == 1);
            Assert.AreEqual("Order with id 2 does not exist", result.Errors[0]);
        }

        [TestMethod]
        public void does_not_return_saved_order()
        {
            Assert.IsNotNull(result);
            Assert.IsNull(result.Result);
        }
    }
}
