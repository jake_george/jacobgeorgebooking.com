﻿using System.Data;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.Business.Commands;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class UpdateOrderItemService_when_update_item_quantity_called_with_invalid_order_id
    {
        private IUpdateOrderItemService _updateOrderItemService;
        private Mock<IOrderRepository> _orderRepository;
        private Mock<ITransaction> _transaction;
        private Mock<IProductRepository> _productRepository;
        private Mock<IUnitOfWork> _currentUnitOfWork;
        private ResultDto<OrderDto> result = null;
        private UpdateOrderItemCommand updateCommand = null;

        [TestInitialize]
        public void Initialise()
        {
            _transaction = new Mock<ITransaction>();
            _orderRepository = new Mock<IOrderRepository>();
            _productRepository = new Mock<IProductRepository>();
            _currentUnitOfWork = new Mock<IUnitOfWork>();
            _updateOrderItemService = new UpdateOrderItemService(_currentUnitOfWork.Object,
                                                         _orderRepository.Object,
                                                         _productRepository.Object);

            updateCommand = new UpdateOrderItemCommand()
            {
                ProductId = 1,
                OrderId = 2,
                Quantity = 5
            };

            _currentUnitOfWork.Setup(c => c.BeginTransaction(It.Is<IsolationLevel>(v => v == IsolationLevel.Serializable))).Returns(_transaction.Object);
                  
            result = _updateOrderItemService.UpdateItemQuantity(updateCommand);
        }


        [TestMethod]
        public void begins_transaction()
        {
            _currentUnitOfWork.Verify(c => c.BeginTransaction(It.Is<IsolationLevel>(v => v == IsolationLevel.Serializable)), Times.Once);
        }

        [TestMethod]
        public void calls_repository_to_get_order()
        {
            _orderRepository.Verify(c => c.GetById(It.Is<int>(v => v == updateCommand.OrderId)), Times.Once);
        }

        [TestMethod]
        public void does_not_call_repository_to_get_product()
        {
            _productRepository.Verify(c => c.GetById(It.IsAny<int>()), Times.Never);
        }

        [TestMethod]
        public void does_not_save_the_unit_of_work()
        {
            _currentUnitOfWork.Verify(c => c.SaveChanges(), Times.Never);
        }

        [TestMethod]
        public void does_not_commit_transaction()
        {
            _transaction.Verify(c => c.Commit(), Times.Never);
        }

        [TestMethod]
        public void returns_error()
        {
            Assert.IsTrue(result.HasErrors);
            Assert.IsTrue(result.Errors.Count == 1);
            Assert.AreEqual("Order with id 2 does not exist", result.Errors[0]);
        }

        [TestMethod]
        public void does_not_return_saved_order()
        {
            Assert.IsNotNull(result);
            Assert.IsNull(result.Result);
        }
    }
}
