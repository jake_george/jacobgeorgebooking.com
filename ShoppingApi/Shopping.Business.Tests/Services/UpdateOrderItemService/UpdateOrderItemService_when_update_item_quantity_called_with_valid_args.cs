﻿using System;
using System.Data;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.Business.Commands;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class UpdateOrderItemService_when_update_item_quantity_called_with_valid_args
    {         
        private IUpdateOrderItemService _updateOrderItemService;
        private Mock<IOrderRepository> _orderRepository;
        private Mock<ITransaction> _transaction;
        private Mock<IProductRepository> _productRepository;
        private Mock<IUnitOfWork> _currentUnitOfWork;
        private ResultDto<OrderDto> result = null;
        private UpdateOrderItemCommand updateCommand = null;

        private Order _orderToReturn = null;
        private Product _productToReturn = null;

        [TestInitialize]
        public void Initialise()
        {
            _transaction = new Mock<ITransaction>();
            _orderRepository = new Mock<IOrderRepository>();
            _productRepository = new Mock<IProductRepository>();
            _currentUnitOfWork = new Mock<IUnitOfWork>();
            _updateOrderItemService = new UpdateOrderItemService(_currentUnitOfWork.Object,
                                                         _orderRepository.Object,
                                                         _productRepository.Object);

            updateCommand = new UpdateOrderItemCommand()
            {
                ProductId = 1,
                OrderId = 2,
                Quantity = 5
            };

            _orderToReturn = new Order()
            {
                Id = updateCommand.OrderId,
                DateCreated = DateTime.Now,
                OrderProducts = new List<OrderProduct>()
            };

            _productToReturn = new Product()
            {
                Id = updateCommand.ProductId,
                Name = "Milk",
                Stock = 14,
                Description = "Semi skimmed milk"
            };

            _currentUnitOfWork.Setup(c => c.BeginTransaction(It.Is<IsolationLevel>(v => v == IsolationLevel.Serializable))).Returns(_transaction.Object);

            _orderRepository.Setup(c => c.GetById(It.Is<int>(v => v == _orderToReturn.Id))).Returns(_orderToReturn);

            _productRepository.Setup(c => c.GetById(It.Is<int>(v => v == _productToReturn.Id))).Returns(_productToReturn);

            result = _updateOrderItemService.UpdateItemQuantity(updateCommand);
        }


        [TestMethod]
        public void begins_transaction()
        {
            _currentUnitOfWork.Verify(c => c.BeginTransaction(It.Is<IsolationLevel>(v => v == IsolationLevel.Serializable)), Times.Once);
        }

        [TestMethod]
        public void calls_repository_to_get_order()
        {
            _orderRepository.Verify(c => c.GetById(It.Is<int>(v => v == _orderToReturn.Id)), Times.Once);
        }


        [TestMethod]
        public void calls_repository_to_get_product()
        {
            _productRepository.Verify(c => c.GetById(It.Is<int>(v => v == _productToReturn.Id)), Times.Once);
        }

        [TestMethod]
        public void saves_the_unit_of_work()
        {
            _currentUnitOfWork.Verify(c => c.SaveChanges(), Times.Once);
        }

        [TestMethod]
        public void commits_transaction()
        {
            _transaction.Verify(c => c.Commit(), Times.Once);
        }

        [TestMethod]
        public void does_not_return_error()
        {
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
        }

        [TestMethod]
        public void returns_saved_order()
        {
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsTrue(result.Result.OrderId == _orderToReturn.Id);
            //Assert.IsTrue(result.Result.Items.ToList().Count == 1);
            //var savedOrderItem = result.Result.Items.SingleOrDefault(c => c.ProductId == _productToReturn.Id
            //                                                           && c.Name == _productToReturn.Name
            //                                                           && c.Description == _productToReturn.Description
            //                                                           && c.Quantity == updateCommand.Quantity);
            //Assert.IsNotNull(savedOrderItem);

        }
    }
}
