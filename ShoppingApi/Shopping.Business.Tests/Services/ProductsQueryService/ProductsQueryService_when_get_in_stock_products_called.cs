﻿using System.Linq;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class ProductsQueryService_when_get_in_stock_products_called
    {
        private Mock<IProductRepository> _productRepository;
        private IProductsQueryService _productsQueryService;
        private ResultDto<List<ProductDto>> result = null;
        private List<Product> productsToReturn = null;

        [TestInitialize]
        public void Initialise()
        {
            _productRepository = new Mock<IProductRepository>();
            _productsQueryService = new ProductsQueryService(_productRepository.Object);

            productsToReturn = new List<Product>()
            {
                new Product(){ Id = 1, Description = "Skimmed Milk", Name = "Milk" },
                new Product(){ Id = 2, Description = "Gala apples", Name = "Apples" },
                new Product(){ Id = 3, Description = "Sweet Oranges", Name = "Oranges" },
                new Product(){ Id = 4, Description = "Wholemeal bread", Name = "Bread" }
            };

            _productRepository.Setup(c => c.GetAllInStockProducts()).Returns(productsToReturn);

            result = _productsQueryService.GetInStockProducts();
        }

        [TestMethod]
        public void calls_repository_to_get_products()
        {
            _productRepository.Verify(a => a.GetAllInStockProducts(), Times.Once);
        }

        [TestMethod]
        public void does_not_return_error()
        {
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
        }

        [TestMethod]
        public void returns_products()
        {
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(productsToReturn.Count, result.Result.Count);

            foreach(var productToReturn in productsToReturn)
            {
               var productInReturnedList = 
                    result.Result.SingleOrDefault(c => c.ProductId == productToReturn.Id
                                                    && c.Name == productToReturn.Name
                                                    && c.Description == productToReturn.Description);

                Assert.IsNotNull(productInReturnedList);
            }
        }
    }
}
