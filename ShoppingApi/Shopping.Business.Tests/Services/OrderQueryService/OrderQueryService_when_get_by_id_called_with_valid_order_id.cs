﻿using System.Linq;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class OrderQueryService_when_get_by_id_called_with_valid_order_id
    {
        private Mock<IOrderRepository> _orderRepository;
        private IOrderQueryService _orderQueryService;
        private ResultDto<OrderDto> result = null;

        private int _orderId = 2;
        private Order orderToReturn = null;

        [TestInitialize]
        public void Initialise()
        {
            _orderRepository = new Mock<IOrderRepository>();
            _orderQueryService = new OrderQueryService(_orderRepository.Object);

            orderToReturn = new Order();
            orderToReturn.Id = _orderId;
            orderToReturn.OrderProducts = new List<OrderProduct>()
            {
                new OrderProduct()
                {
                    OrderId = _orderId,
                    ProductId = 1,
                    Quantity = 11,
                    Product = new Product()
                    {
                        Id = 1,
                        Name = "Milk",
                        Description = "Skimmed Milk",
                        Stock = 3
                    }
                }
            };

            _orderRepository.Setup(c => c.GetById(It.Is<int>(v => v == _orderId))).Returns(orderToReturn);

            result = _orderQueryService.GetOrder(_orderId);
        }

        [TestMethod]
        public void calls_repository_to_get_order()
        {
            _orderRepository.Verify(a => a.GetById(It.Is<int>(c => c == _orderId)), Times.Once);
        }

        [TestMethod]
        public void does_not_return_error()
        {
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
        }

        [TestMethod]
        public void returns_requested_order()
        {
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(orderToReturn.Id, result.Result.OrderId);
            Assert.IsTrue(result.Result.Items.Count == orderToReturn.OrderProducts.Count);

            foreach (var orderProduct in orderToReturn.OrderProducts)
            {
                var basketItemForProduct = result.Result.Items.SingleOrDefault(v => v.ProductId == orderProduct.ProductId
                                                                                 && v.Quantity == orderProduct.Quantity
                                                                                 && v.Name == orderProduct.Product.Name
                                                                                 && v.Description == orderProduct.Product.Description);

                Assert.IsNotNull(basketItemForProduct);
            }
        }
    }
}
