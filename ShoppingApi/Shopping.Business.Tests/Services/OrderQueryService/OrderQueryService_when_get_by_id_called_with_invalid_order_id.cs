﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.Repositories;

namespace Shopping.Business.Tests
{
    [TestClass]
    public class OrderQueryService_when_get_by_id_called_with_invalid_order_id
    {
        private Mock<IOrderRepository> _orderRepository;
        private IOrderQueryService _orderQueryService;
        private ResultDto<OrderDto> result = null;
        private int _orderId = 2;

        [TestInitialize]
        public void Initialise()
        {
            _orderRepository = new Mock<IOrderRepository>();
            _orderQueryService = new OrderQueryService(_orderRepository.Object);
        
            result = _orderQueryService.GetOrder(_orderId);
        }

        [TestMethod]
        public void calls_repository_to_get_order()
        {
            _orderRepository.Verify(a => a.GetById(It.Is<int>(c => c == _orderId)), Times.Once);
        }

        [TestMethod]
        public void returns_error()
        {
            Assert.IsTrue(result.HasErrors);
            Assert.IsTrue(result.Errors.Count == 1);
            Assert.AreEqual("Order with id 2 does not exist", result.Errors[0]);
        }

        [TestMethod]
        public void does_not_return_requested_order()
        {
            Assert.IsNotNull(result);
            Assert.IsNull(result.Result);
        }
    }
}
