﻿using System;
using System.Collections.Generic;

namespace Shopping.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
