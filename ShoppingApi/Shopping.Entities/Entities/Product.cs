﻿using System.Collections.Generic;

namespace Shopping.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public int Stock { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
