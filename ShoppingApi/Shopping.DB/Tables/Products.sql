﻿CREATE TABLE [dbo].[Products]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [Name] VARCHAR(70) NOT NULL, 
    [Stock] INT NOT NULL,
	[Description] [varchar](500) NOT NULL
)
