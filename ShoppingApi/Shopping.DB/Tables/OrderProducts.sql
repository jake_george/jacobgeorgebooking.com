﻿CREATE TABLE [dbo].[OrderProducts]
(
    [OrderId] INT NOT NULL, 
    [ProductId] INT NOT NULL, 
    [Quantity] INT NOT NULL
	CONSTRAINT [FK_Qst.OrderProducts_dbo.Products_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([Id]),
	CONSTRAINT [FK_Qst.OrderProducts_dbo.Orders_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([Id]), 
    CONSTRAINT [PK_OrderProducts] PRIMARY KEY ([OrderId], [ProductId]) 
)
