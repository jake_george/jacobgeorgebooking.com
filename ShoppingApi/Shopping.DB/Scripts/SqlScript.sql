﻿CREATE TABLE [dbo].[Orders]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [DateCreated] DATETIME NOT NULL
)

GO

CREATE TABLE [dbo].[Products]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [Name] VARCHAR(70) NOT NULL, 
    [Stock] INT NOT NULL,
	[Description] [varchar](500) NOT NULL
)

GO


CREATE TABLE [dbo].[OrderProducts]
(
    [OrderId] INT NOT NULL, 
    [ProductId] INT NOT NULL, 
    [Quantity] INT NOT NULL
	CONSTRAINT [FK_Qst.OrderProducts_dbo.Products_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([Id]),
	CONSTRAINT [FK_Qst.OrderProducts_dbo.Orders_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([Id]), 
    CONSTRAINT [PK_OrderProducts] PRIMARY KEY ([OrderId], [ProductId]) 
)

GO

INSERT INTO Products(Name, Stock, Description) VALUES('Milk', 4, 'Pasteurised standardised homogenised whole milk.')
GO
INSERT INTO Products(Name, Stock, Description) VALUES('Apples', 11, 'Unique and distinctive fizzy flavoured apples')
GO
INSERT INTO Products(Name, Stock, Description) VALUES('Oranges', 5, 'Carefully grown in citrus orchards, bursting with sweet, juicy flavour')
GO
INSERT INTO Products(Name, Stock, Description) VALUES('Grapes', 10, 'Carefully grown in vineyards for a burst of deep, rich sweetness')
GO
INSERT INTO Products(Name, Stock, Description) VALUES('Bread', 12, 'A tasty wholemeal traditional tin loaf with a soft centre')
GO
