﻿using System.Linq;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Unity;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.Settings;
using Shopping.Business.IntegrationTests.IoC;
using Shopping.Business.IntegrationTests.DataAccess;
using Shopping.Business.IntegrationTests.TestObjects;

namespace Shopping.Business.IntegrationTests
{
    [TestClass]
    public class OrderQueryService_when_get_order_by_id_called
    {
        private IOrderQueryService _orderQueryService;
        private ResultDto<OrderDto> result = null;
        private TestOrder _testOrder = null;
        private List<TestProduct> productsToCreate;
        private TestProduct _productToAddToOrder;
        private TestProductDataService _testProductDataService;
        private TestOrderDataService _testOrderDataService;
        [TestInitialize]
        public void Initialise()
        {
            var container = IntegrationTestsUnityConfig.RegisterComponents();
            var settings = container.Resolve<ISettings>();            

            productsToCreate = new List<TestProduct>()
            {
                new TestProduct(){ Name = "Pomergranate", Description = "Pomergranate desc", Stock = 2 },
                new TestProduct(){ Name = "Water melon", Description = "water melons", Stock = 3 }
            };

            _testProductDataService = new TestProductDataService(settings);
            _testProductDataService.CreateProducts(productsToCreate);

            _testOrderDataService = new TestOrderDataService(settings);
            _testOrder = _testOrderDataService.CreateOrder();

            _productToAddToOrder = productsToCreate.FirstOrDefault();
            _testOrderDataService.AddItemToOrder(_testOrder.Id, _productToAddToOrder.Id, _productToAddToOrder.Stock);

            _orderQueryService = container.Resolve<IOrderQueryService>();
            result = _orderQueryService.GetOrder(_testOrder.Id);
        }

        [TestMethod]
        public void returns_order()
        {
            Assert.AreEqual(_testOrder.Id, result.Result.OrderId);

            Assert.AreEqual(1, result.Result.Items.Count);

            var orderItem = result.Result.Items.FirstOrDefault();

            Assert.AreEqual(_productToAddToOrder.Id, orderItem.ProductId);
            Assert.AreEqual(_productToAddToOrder.Name, orderItem.Name);
            Assert.AreEqual(_productToAddToOrder.Description, orderItem.Description);
            Assert.AreEqual(_productToAddToOrder.Stock, orderItem.Quantity);  
        }

        [TestCleanup]
        public void delete_test_data()
        {
            _testOrderDataService.DeleteOrder(_testOrder.Id);

            _testProductDataService.DeleteProducts(productsToCreate);
        }
    }
}
