﻿using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Unity;

using Shopping.Entities;
using Shopping.Business.Services;
using Shopping.DataAccess.Settings;
using Shopping.Business.IntegrationTests.IoC;
using Shopping.Business.IntegrationTests.DataAccess;
using Shopping.Business.IntegrationTests.TestObjects;

namespace Shopping.Business.IntegrationTests
{
    [TestClass]
    public class ClearOrderService_when_clear_items_called
    {
        private IClearOrderService _clearOrderService;
        private TestOrder _clearedOrder = null;
        private List<TestProduct> productsToCreate;
        private TestProductDataService _testProductDataService;
        private TestOrderDataService _testOrderDataService;
        [TestInitialize]
        public void Initialise()
        {
            var container = IntegrationTestsUnityConfig.RegisterComponents();
            var settings = container.Resolve<ISettings>();            

            productsToCreate = new List<TestProduct>()
            {
                new TestProduct(){ Name = "Pomergranate", Description = "Pomergranate desc", Stock = 2 },
                new TestProduct(){ Name = "Water melon", Description = "water melons", Stock = 3 }
            };

            _testProductDataService = new TestProductDataService(settings);
            _testProductDataService.CreateProducts(productsToCreate);

            _testOrderDataService = new TestOrderDataService(settings);
             var newOrder = _testOrderDataService.CreateOrder();

            _testOrderDataService.AddItemToOrder(newOrder.Id, productsToCreate[0].Id, productsToCreate[0].Stock);
            _testOrderDataService.AddItemToOrder(newOrder.Id, productsToCreate[1].Id, productsToCreate[1].Stock);

            _clearOrderService = container.Resolve<IClearOrderService>();
            _clearOrderService.ClearItems(newOrder.Id);

            _clearedOrder = _testOrderDataService.GetOrder(newOrder.Id);
        }

        [TestMethod]
        public void clears_order()
        {
            Assert.IsTrue(_clearedOrder.Items.Count == 0);
        }

        [TestCleanup]
        public void delete_test_data()
        {
            _testOrderDataService.DeleteOrder(_clearedOrder.Id);

            _testProductDataService.DeleteProducts(productsToCreate);
        }
    }
}
