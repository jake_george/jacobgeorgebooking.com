﻿using Shopping.Business.Services;
using Shopping.DataAccess.Settings;
using Shopping.DataAccess.UnitOfWork;
using Shopping.DataAccess.Repositories;

using Unity;
using Unity.Lifetime;

namespace Shopping.Business.IntegrationTests.IoC
{
    public static class IntegrationTestsUnityConfig
    {
        public static IUnityContainer RegisterComponents()
        {       
            var container = new UnityContainer();

            container.RegisterSingleton<ISettings, Settings>();

            container.RegisterType<IUnitOfWork, EntityFrameworkUnitOfWork>(new PerResolveLifetimeManager());

            container.RegisterType<IOrderRepository, OrderRepository>(new TransientLifetimeManager());

            container.RegisterType<IProductRepository, ProductRepository>(new TransientLifetimeManager());

            container.RegisterType<IProductsQueryService, ProductsQueryService>(new TransientLifetimeManager());            

            container.RegisterType<IOrderQueryService, OrderQueryService>(new TransientLifetimeManager());

            container.RegisterType<ICreateOrderService, CreateOrderService>(new TransientLifetimeManager());

            container.RegisterType<IUpdateOrderItemService, UpdateOrderItemService>(new TransientLifetimeManager());

            container.RegisterType<IClearOrderService, ClearOrderService>(new TransientLifetimeManager());

            return container;
        }
    }
}