﻿using System.Linq;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Unity;

using Shopping.Entities;
using Shopping.Business.Services;
using Shopping.Business.Commands;
using Shopping.DataAccess.Settings;
using Shopping.Business.IntegrationTests.IoC;
using Shopping.Business.IntegrationTests.DataAccess;
using Shopping.Business.IntegrationTests.TestObjects;

namespace Shopping.Business.IntegrationTests
{
    [TestClass]
    public class UpdateOrderItemService_when_update_item_called
    {
        private IUpdateOrderItemService _updateOrderItemService;
        private TestOrder _updatedOrder = null;
        private UpdateOrderItemCommand updateItemCommand = null;
        private List<TestProduct> productsToCreate;
        private TestProduct productToUpdateQuantityFor = null;
        private TestProductDataService _testProductDataService;
        private TestOrderDataService _testOrderDataService;
        [TestInitialize]
        public void Initialise()
        {
            var container = IntegrationTestsUnityConfig.RegisterComponents();
            var settings = container.Resolve<ISettings>();            

            productsToCreate = new List<TestProduct>()
            {
                new TestProduct(){ Name = "Bread", Description = "Wholemeal bread", Stock = 4 },
                new TestProduct(){ Name = "Jam", Description = "Strawberry jam", Stock = 5 }
            };

            _testProductDataService = new TestProductDataService(settings);
            _testProductDataService.CreateProducts(productsToCreate);

            _testOrderDataService = new TestOrderDataService(settings);
             var newOrder = _testOrderDataService.CreateOrder();

             productToUpdateQuantityFor = productsToCreate[0];

            _testOrderDataService.AddItemToOrder(newOrder.Id, productsToCreate[0].Id, 1);
            _testOrderDataService.AddItemToOrder(newOrder.Id, productsToCreate[1].Id, 1);

            _updateOrderItemService = container.Resolve<IUpdateOrderItemService>();

            updateItemCommand = new UpdateOrderItemCommand();
            updateItemCommand.OrderId = newOrder.Id;
            updateItemCommand.ProductId = productToUpdateQuantityFor.Id;
            updateItemCommand.Quantity = 3;

            _updateOrderItemService.UpdateItemQuantity(updateItemCommand);

            _updatedOrder = _testOrderDataService.GetOrder(newOrder.Id);
        }

        [TestMethod]
        public void updates_order_item_quantity()
        {
            Assert.AreEqual(2, _updatedOrder.Items.Count);

            var updateOrderRecord = _updatedOrder.Items.SingleOrDefault(c => c.ProductId == productToUpdateQuantityFor.Id
                                                          && c.Name == productToUpdateQuantityFor.Name
                                                          && c.Description == productToUpdateQuantityFor.Description);

            Assert.IsNotNull(updateOrderRecord);
            Assert.AreEqual(updateItemCommand.Quantity, updateOrderRecord.Quantity);
        }

        [TestCleanup]
        public void delete_test_data()
        {
            _testOrderDataService.DeleteOrder(_updatedOrder.Id);

            _testProductDataService.DeleteProducts(productsToCreate);
        }
    }
}
