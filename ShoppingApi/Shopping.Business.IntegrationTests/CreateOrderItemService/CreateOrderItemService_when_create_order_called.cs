﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Unity;

using Shopping.Business.Services;
using Shopping.DataAccess.Settings;
using Shopping.Business.IntegrationTests.IoC;
using Shopping.Business.IntegrationTests.DataAccess;
using Shopping.Business.IntegrationTests.TestObjects;

namespace Shopping.Business.IntegrationTests
{
    [TestClass]
    public class CreateOrderItemService_when_create_order_called
    {
        private ICreateOrderService _createOrderService;
        private TestOrder _orderToGet = null;
        private TestOrderDataService _testOrderDataService;
        [TestInitialize]
        public void Initialise()
        {
            var container = IntegrationTestsUnityConfig.RegisterComponents();
            var settings = container.Resolve<ISettings>();

            _createOrderService = container.Resolve<ICreateOrderService>();
            var newOrder = _createOrderService.CreateOrder();

            _testOrderDataService = new TestOrderDataService(settings);
            _orderToGet = _testOrderDataService.GetOrder(newOrder.Result.OrderId);
        }

        [TestMethod]
        public void creates_new_order()
        {
            Assert.IsNotNull(_orderToGet);
            Assert.IsTrue(_orderToGet.Id > 0);
        }

        [TestCleanup]
        public void delete_test_data()
        {
            _testOrderDataService.DeleteOrder(_orderToGet.Id);
        }
    }
}
