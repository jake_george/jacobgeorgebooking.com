﻿namespace Shopping.Business.IntegrationTests.TestObjects
{
    public class TestProduct
    {
        public int Id { get; set; }
        public int Stock { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }     
    }
}
