﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping.Business.IntegrationTests.TestObjects
{
    public class TestOrder
    {
        public int Id { get; set; }
        public List<TestOrderItem> Items { get; set; }        
    }

    public class TestOrderItem
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
    }

}
