﻿using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using Shopping.DataAccess.Settings;
using Shopping.Business.IntegrationTests.TestObjects;

namespace Shopping.Business.IntegrationTests.DataAccess
{
    public class TestProductDataService
    {
        private AdoNetDataAccess _dataAccess = null;
        public TestProductDataService(ISettings settings)
        {
            _dataAccess = new AdoNetDataAccess(settings.ConnectionString);
        }

        public void CreateProducts(List<TestProduct> products)
        {
            foreach (var product in products)
            {
                var idOutputParam = new SqlParameter("@id", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                _dataAccess.ExecuteNonQuery($"INSERT INTO Products (Name, Stock, Description) VALUES (@name, @stock, @description) SELECT @id = SCOPE_IDENTITY()",
                               new SqlParameter[]
                               {
                                    idOutputParam,
                                    new SqlParameter("@name", product.Name),
                                    new SqlParameter("@stock", product.Stock),
                                    new SqlParameter("@description", product.Description)
                               });

                product.Id = int.Parse(idOutputParam.Value.ToString());
            }
        }

        public void DeleteProducts(List<TestProduct> products)
        {
            foreach (var product in products)
            {
                _dataAccess.ExecuteNonQuery($"DELETE FROM Products WHERE Id = @id",
                               new SqlParameter[]
                               {
                                    new SqlParameter("@id", product.Id)
                               });
            }
        }
    }
}
