﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using Shopping.DataAccess.Settings;
using Shopping.Business.IntegrationTests.TestObjects;

namespace Shopping.Business.IntegrationTests.DataAccess
{
    public class TestOrderDataService
    {
        private AdoNetDataAccess _dataAccess = null;
        public TestOrderDataService(ISettings settings)
        {
            _dataAccess = new AdoNetDataAccess(settings.ConnectionString);
        }

        public TestOrder GetOrder(int id)
        {
            var orderReader = _dataAccess.GetReader($"SELECT ord.Id, ordprd.Quantity, prd.Id as [ProductId], prd.[Name] as ProductName, prd.[Description] as [ProductDescription], ordprd.Quantity FROM Orders ord LEFT OUTER JOIN OrderProducts ordprd ON ord.Id = ordprd.OrderId LEFT OUTER JOIN Products prd ON prd.Id = ordprd.ProductId WHERE ord.Id = @orderId",
                            new SqlParameter[] { new SqlParameter("@orderId", id) });

            TestOrder newOrder = null;
            while (orderReader.Read())
            {
                if (newOrder == null)
                {
                    newOrder = new TestOrder();
                    newOrder.Items = new List<TestOrderItem>();
                    newOrder.Id = (int)orderReader["Id"];
                }

                if (orderReader["ProductId"] != DBNull.Value)
                {
                    var testOrderItem = new TestOrderItem();
                    testOrderItem.ProductId = (int)orderReader["ProductId"];
                    testOrderItem.Name = (string)orderReader["ProductName"];
                    testOrderItem.Description = (string)orderReader["ProductDescription"];
                    testOrderItem.Quantity = (int)orderReader["Quantity"];

                    newOrder.Items.Add(testOrderItem);
                }                
            }           

            return newOrder;
        }


        public TestOrder CreateOrder()
        {
            var idOutputParam = new SqlParameter("@id", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            _dataAccess.ExecuteNonQuery($"INSERT INTO Orders (DateCreated) VALUES (GETDATE()) SELECT @id = SCOPE_IDENTITY()",
                           new SqlParameter[] { idOutputParam });

            var newOrder = new TestOrder();
            newOrder.Id = int.Parse(idOutputParam.Value.ToString());

            return newOrder;
        }

        public void AddItemToOrder(int orderId, int productId, int quantity)
        {
            _dataAccess.ExecuteNonQuery($"INSERT INTO OrderProducts (OrderId, ProductId, Quantity) VALUES (@orderId, @productId, @quantity)",
                           new SqlParameter[]
                           {
                                    new SqlParameter("@orderId", orderId),
                                    new SqlParameter("@productId", productId),
                                    new SqlParameter("@quantity", quantity)
                           });
        }

        public void DeleteOrder(int orderid)
        {
            _dataAccess.ExecuteNonQuery($"DELETE FROM OrderProducts WHERE OrderId = @id",
                              new SqlParameter[] { new SqlParameter("@id", orderid) });

            _dataAccess.ExecuteNonQuery($"DELETE FROM Orders WHERE Id = @id",
                              new SqlParameter[] { new SqlParameter("@id", orderid) });
        }
    }
}
