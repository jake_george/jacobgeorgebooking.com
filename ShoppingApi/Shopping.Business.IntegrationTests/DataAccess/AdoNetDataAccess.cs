﻿using System.Data;
using System.Data.SqlClient;

namespace Shopping.Business.IntegrationTests.DataAccess
{
    public class AdoNetDataAccess
    {
        private string _connectionString;
        public AdoNetDataAccess(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDataReader GetReader(string storedProcedureName, SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = storedProcedureName;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }

                    using (var dataAdapter = new SqlDataAdapter(command))
                    {
                        var dataSet = new DataSet();

                        dataAdapter.Fill(dataSet);

                        return dataSet.CreateDataReader();
                    }
                }
            }
        }

        public SqlParameterCollection ExecuteNonQuery(string commandText, SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;

                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }

                    command.ExecuteNonQuery();

                    return command.Parameters;
                }
            }
        }
    }
}
