﻿using System.Linq;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Unity;

using Shopping.Entities;
using Shopping.Business.Dtos;
using Shopping.Business.Services;
using Shopping.DataAccess.Settings;
using Shopping.Business.IntegrationTests.IoC;
using Shopping.Business.IntegrationTests.DataAccess;
using Shopping.Business.IntegrationTests.TestObjects;

namespace Shopping.Business.IntegrationTests
{
    [TestClass]
    public class ProductsQueryService_when_get_in_stock_products_called
    {
        private IProductsQueryService _productsQueryService;
        private ResultDto<List<ProductDto>> result = null;
        private List<TestProduct> productsToCreate;
        private TestProductDataService _testProductDataService;

        [TestInitialize]
        public void Initialise()
        {
            var container = IntegrationTestsUnityConfig.RegisterComponents();
            var settings = container.Resolve<ISettings>();            

            productsToCreate = new List<TestProduct>()
            {
                new TestProduct(){ Name = "Milk", Description = "Semi skimmed milk", Stock = 0 },
                new TestProduct(){ Name = "Grapes", Description = "Sweet grapes", Stock = 0 },
                new TestProduct(){ Name = "Apples", Description = "Gala apples", Stock = 33 },
                new TestProduct(){ Name = "Melons", Description = "Summer melons", Stock = 11 }
            };

            _testProductDataService = new TestProductDataService(settings);
            _testProductDataService.CreateProducts(productsToCreate);

            _productsQueryService = container.Resolve<IProductsQueryService>();
            result = _productsQueryService.GetInStockProducts();

            var productIds = productsToCreate.Select(c => c.Id).ToList();
            result.Result = result.Result.Where(c => productIds.Contains(c.ProductId)).ToList();
        }

        [TestMethod]
        public void returns_in_stock_products()
        {
            var inStockProducts = productsToCreate.Where(c => c.Stock > 0).ToList();

            Assert.AreEqual(inStockProducts.Count, result.Result.Count);

            foreach (var inStockProduct in inStockProducts)
            {
                var resultInstance = 
                result.Result.SingleOrDefault(c => c.ProductId == inStockProduct.Id
                                                && c.Name == inStockProduct.Name
                                                && c.Description == inStockProduct.Description);

                Assert.IsNotNull(resultInstance);
            }
        }

        [TestCleanup]
        public void delete_test_data()
        {
            _testProductDataService.DeleteProducts(productsToCreate);
        }
    }
}
