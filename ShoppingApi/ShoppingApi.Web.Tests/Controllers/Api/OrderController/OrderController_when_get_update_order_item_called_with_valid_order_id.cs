﻿using System.Net;
using System.Linq;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using ShoppingApi.Controllers.Api;
using Shopping.Business.Commands;

namespace ShoppingApi.Web.Tests
{
    [TestClass]
    public class OrderController_when_get_update_order_item_called_with_valid_order_id
    {
        private OrderController _orderController;
        private Mock<IOrderQueryService> _orderQueryService;
        private Mock<IClearOrderService> _clearOrderService;
        private Mock<ICreateOrderService> _createOrderService;
        private Mock<IUpdateOrderItemService> _updateOrderItemService;
        private int _orderId = 2;
        private UpdateOrderItemCommand _updateCommand;
        private FormattedContentResult<ResultDto<OrderDto>> _response;
        private ResultDto<OrderDto> _orderToReturn;

        [TestInitialize]
        public void Initialise()
        {
            _orderQueryService = new Mock<IOrderQueryService>();
            _clearOrderService = new Mock<IClearOrderService>();
            _createOrderService = new Mock<ICreateOrderService>();
            _updateOrderItemService = new Mock<IUpdateOrderItemService>();

            _orderController = new OrderController(_orderQueryService.Object,
                                                   _clearOrderService.Object,
                                                   _createOrderService.Object,
                                                   _updateOrderItemService.Object);

            _orderController.Request = new HttpRequestMessage();
            _orderController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _updateCommand = new UpdateOrderItemCommand()
            {
                OrderId = _orderId,
                ProductId = 2,
                Quantity = 3
            };

            _orderToReturn = new ResultDto<OrderDto>()
            {
                Result = new OrderDto
                {
                    OrderId = 1,
                    Items = new List<OrderItemDto>()
                    {
                        new OrderItemDto() { ProductId = 1, Description = "Skimmed Milk", Name = "Milk", Quantity = 2 },
                        new OrderItemDto() { ProductId = 2, Description = "Wholemeal bread", Name = "Bread", Quantity = 3 },
                    }
                }
            };

            _updateOrderItemService.Setup(c => c.UpdateItemQuantity(It.Is<UpdateOrderItemCommand>(v => v.OrderId == _updateCommand.OrderId
                                                                                                    && v.ProductId == _updateCommand.ProductId
                                                                                                    && v.Quantity == _updateCommand.Quantity))).Returns(_orderToReturn);

            _response = (FormattedContentResult<ResultDto<OrderDto>>)_orderController.update(_updateCommand, _orderId);
        }

        [TestMethod]
        public void calls_service_to_update_order_items()
        {
            _updateOrderItemService.Verify(c => c.UpdateItemQuantity(It.Is<UpdateOrderItemCommand>(v => v.OrderId == _updateCommand.OrderId
                                                                                                     && v.ProductId == _updateCommand.ProductId
                                                                                                     && v.Quantity == _updateCommand.Quantity)), Times.Once);
        }

        [TestMethod]
        public void returns_ok_status_code()
        {
            Assert.AreEqual(HttpStatusCode.OK, _response.StatusCode);
        }

        [TestMethod]
        public void returns_correct_result()
        {
            Assert.AreEqual(_orderToReturn.HasErrors, _response.Content.HasErrors);
            Assert.AreEqual(_orderToReturn.Errors.Count, _response.Content.Errors.Count);
            Assert.IsNotNull(_orderToReturn.Result);
            Assert.AreEqual(_orderToReturn.Result.OrderId, _response.Content.Result.OrderId);
            Assert.AreEqual(_orderToReturn.Result.OrderId, _response.Content.Result.OrderId);
            Assert.AreEqual(_orderToReturn.Result.Items.Count, _response.Content.Result.Items.Count);

            foreach(var orderItem in _orderToReturn.Result.Items)
            {
                var returnedItem = _response.Content.Result.Items.SingleOrDefault(c => c.ProductId == orderItem.ProductId
                                                                                     && c.Name == orderItem.Name
                                                                                     && c.Description == orderItem.Description
                                                                                     && c.Quantity == orderItem.Quantity);
                Assert.IsNotNull(returnedItem);
            }
        }
    }
}
