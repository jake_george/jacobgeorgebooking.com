﻿using System.Net;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using ShoppingApi.Controllers.Api;
using Shopping.Business.Commands;

namespace ShoppingApi.Web.Tests
{
    [TestClass]
    public class OrderController_when_get_update_order_item_called_with_invalid_order_id
    {
        private OrderController _orderController;
        private Mock<IOrderQueryService> _orderQueryService;
        private Mock<IClearOrderService> _clearOrderService;
        private Mock<ICreateOrderService> _createOrderService;
        private Mock<IUpdateOrderItemService> _updateOrderItemService;
        private int _orderId = 22;
        private UpdateOrderItemCommand _updateCommand;
        private FormattedContentResult<ResultDto<OrderDto>> _response;
        private ResultDto<OrderDto> _orderToReturn;

        [TestInitialize]
        public void Initialise()
        {
            _orderQueryService = new Mock<IOrderQueryService>();
            _clearOrderService = new Mock<IClearOrderService>();
            _createOrderService = new Mock<ICreateOrderService>();
            _updateOrderItemService = new Mock<IUpdateOrderItemService>();

            _orderController = new OrderController(_orderQueryService.Object,
                                                   _clearOrderService.Object,
                                                   _createOrderService.Object,
                                                   _updateOrderItemService.Object);

            _orderController.Request = new HttpRequestMessage();
            _orderController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _updateCommand = new UpdateOrderItemCommand()
            {
                OrderId = _orderId,
                ProductId = 2,
                Quantity = 3
            };

            _orderToReturn = new ResultDto<OrderDto>()
            {
                Result = null,
                Errors = new List<string>()
                {
                    $"Could not find order with id {_orderId}"
                }
            };

            _updateOrderItemService.Setup(c => c.UpdateItemQuantity(It.Is<UpdateOrderItemCommand>(v => v.OrderId == _updateCommand.OrderId
                                                                                                    && v.ProductId == _updateCommand.ProductId
                                                                                                    && v.Quantity == _updateCommand.Quantity))).Returns(_orderToReturn);

            _response = (FormattedContentResult<ResultDto<OrderDto>>)_orderController.update(_updateCommand, _orderId);
        }

        [TestMethod]
        public void calls_service_to_update_order_items()
        {
            _updateOrderItemService.Verify(c => c.UpdateItemQuantity(It.Is<UpdateOrderItemCommand>(v => v.OrderId == _updateCommand.OrderId
                                                                                                     && v.ProductId == _updateCommand.ProductId
                                                                                                     && v.Quantity == _updateCommand.Quantity)), Times.Once);
        }

        [TestMethod]
        public void returns_bad_request_status_code()
        {
            Assert.AreEqual(HttpStatusCode.BadRequest, _response.StatusCode);
        }

        [TestMethod]
        public void returns_error()
        {
            Assert.IsTrue(_orderToReturn.HasErrors);
            Assert.IsTrue(_orderToReturn.Errors.Count == 1);
            Assert.AreEqual("Could not find order with id 22", _orderToReturn.Errors[0]);
        }

        [TestMethod]
        public void returns_null_result()
        {
            Assert.IsNull(_orderToReturn.Result);
        }
    }
}
