﻿using System.Net;
using System.Linq;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using ShoppingApi.Controllers.Api;

namespace ShoppingApi.Web.Tests
{
    [TestClass]
    public class OrderController_when_get_order_called_with_valid_order_id
    {
        private OrderController _orderController;
        private Mock<IOrderQueryService> _orderQueryService;
        private Mock<IClearOrderService> _clearOrderService;
        private Mock<ICreateOrderService> _createOrderService;
        private Mock<IUpdateOrderItemService> _updateOrderItemService;
        private int _orderId = 2;
        private FormattedContentResult<ResultDto<OrderDto>> _response;
        private ResultDto<OrderDto> _orderToReturn;

        [TestInitialize]
        public void Initialise()
        {
            _orderQueryService = new Mock<IOrderQueryService>();
            _clearOrderService = new Mock<IClearOrderService>();
            _createOrderService = new Mock<ICreateOrderService>();
            _updateOrderItemService = new Mock<IUpdateOrderItemService>();

            _orderController = new OrderController(_orderQueryService.Object,
                                                   _clearOrderService.Object,
                                                   _createOrderService.Object,
                                                   _updateOrderItemService.Object);

            _orderController.Request = new HttpRequestMessage();
            _orderController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _orderToReturn = new ResultDto<OrderDto>()
            {
                Result = new OrderDto
                {
                    OrderId = 1,
                    Items = new List<OrderItemDto>()
                    {
                        new OrderItemDto() { ProductId = 1, Description = "Skimmed Milk", Name = "Milk", Quantity = 2 },
                        new OrderItemDto() { ProductId = 2, Description = "Wholemeal bread", Name = "Bread", Quantity = 4 },
                    }
                }
            };

            _orderQueryService.Setup(c => c.GetOrder(It.Is<int>(v => v == _orderId))).Returns(_orderToReturn);
            _response = (FormattedContentResult<ResultDto<OrderDto>>)_orderController.Get(_orderId);
        }

        [TestMethod]
        public void calls_service_to_get_order()
        {
            _orderQueryService.Verify(a => a.GetOrder(It.Is<int>(c => c == _orderId)), Times.Once);
        }

        [TestMethod]
        public void returns_ok_status_code()
        {
            Assert.AreEqual(HttpStatusCode.OK, _response.StatusCode);
        }

        [TestMethod]
        public void returns_correct_result()
        {
            Assert.AreEqual(_orderToReturn.HasErrors, _response.Content.HasErrors);
            Assert.AreEqual(_orderToReturn.Errors.Count, _response.Content.Errors.Count);
            Assert.IsNotNull(_orderToReturn.Result);
            Assert.AreEqual(_orderToReturn.Result.OrderId, _response.Content.Result.OrderId);
            Assert.AreEqual(_orderToReturn.Result.OrderId, _response.Content.Result.OrderId);
            Assert.AreEqual(_orderToReturn.Result.Items.Count, _response.Content.Result.Items.Count);

            foreach(var orderItem in _orderToReturn.Result.Items)
            {
                var returnedItem = _response.Content.Result.Items.SingleOrDefault(c => c.ProductId == orderItem.ProductId
                                                                                     && c.Name == orderItem.Name
                                                                                     && c.Description == orderItem.Description
                                                                                     && c.Quantity == orderItem.Quantity);
                Assert.IsNotNull(returnedItem);
            }
        }
    }
}
