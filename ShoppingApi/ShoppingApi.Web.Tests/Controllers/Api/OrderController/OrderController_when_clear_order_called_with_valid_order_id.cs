﻿using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using ShoppingApi.Controllers.Api;
using System.Net;

namespace ShoppingApi.Web.Tests
{
    [TestClass]
    public class OrderController_when_clear_order_called_with_valid_order_id
    {
        private OrderController _orderController;
        private Mock<IOrderQueryService> _orderQueryService;
        private Mock<IClearOrderService> _clearOrderService;
        private Mock<ICreateOrderService> _createOrderService;
        private Mock<IUpdateOrderItemService> _updateOrderItemService;
        private int _orderId = 2;
        private FormattedContentResult<ResultDto<OrderDto>> _response;
        private ResultDto<OrderDto> _orderToReturn;

        [TestInitialize]
        public void Initialise()
        {
            _orderQueryService = new Mock<IOrderQueryService>();
            _clearOrderService = new Mock<IClearOrderService>();
            _createOrderService = new Mock<ICreateOrderService>();
            _updateOrderItemService = new Mock<IUpdateOrderItemService>();

            _orderController = new OrderController(_orderQueryService.Object,
                                                   _clearOrderService.Object,
                                                   _createOrderService.Object,
                                                   _updateOrderItemService.Object);

            _orderController.Request = new HttpRequestMessage();
            _orderController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _orderToReturn = new ResultDto<OrderDto>()
            {
                Result = new OrderDto
                {
                    OrderId = 1,
                    Items = new List<OrderItemDto>()
                }
            };

            _clearOrderService.Setup(c => c.ClearItems(It.Is<int>(v => v == _orderId))).Returns(_orderToReturn);
            _response = (FormattedContentResult<ResultDto<OrderDto>>)_orderController.clear(_orderId);
        }

        [TestMethod]
        public void calls_service_to_clear_order()
        {
            _clearOrderService.Verify(a => a.ClearItems(It.Is<int>(c => c == _orderId)), Times.Once);
        }

        [TestMethod]
        public void returns_ok_status_code()
        {
            Assert.AreEqual(HttpStatusCode.OK, _response.StatusCode);
        }

        [TestMethod]
        public void returns_correct_result()
        {
            Assert.AreEqual(_orderToReturn.HasErrors, _response.Content.HasErrors);
            Assert.AreEqual(_orderToReturn.Errors.Count, _response.Content.Errors.Count);
            Assert.IsNotNull(_orderToReturn.Result);
            Assert.AreEqual(_orderToReturn.Result.OrderId, _response.Content.Result.OrderId);
            Assert.AreEqual(_orderToReturn.Result.OrderId, _response.Content.Result.OrderId);
            Assert.AreEqual(_orderToReturn.Result.Items.Count, _response.Content.Result.Items.Count);
        }
    }
}
