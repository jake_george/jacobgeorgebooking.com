﻿using System.Net;
using System.Linq;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Collections.Generic;

using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shopping.Business.Dtos;
using Shopping.Business.Services;
using ShoppingApi.Controllers.Api;

namespace ShoppingApi.Web.Tests
{
    [TestClass]
    public class ProductsController_when_get_products_called
    {
        private ProductsController _productsController;
        private Mock<IProductsQueryService> _productsQueryService;      
        private FormattedContentResult<ResultDto<List<ProductDto>>> _response;
        private ResultDto<List<ProductDto>> _productsToReturn;

        [TestInitialize]
        public void Initialise()
        {
            _productsQueryService = new Mock<IProductsQueryService>();
            _productsController = new ProductsController(_productsQueryService.Object);
            _productsController.Request = new HttpRequestMessage();
            _productsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _productsToReturn = new ResultDto<List<ProductDto>>()
            {
                Result = new List<ProductDto>()
                {
                 new ProductDto() { ProductId = 1,  Description = "Skimmed Milk",  Name = "Milk" },
                 new ProductDto() { ProductId = 2,  Description = "Whole meal bread",  Name = "Bread" },
                 new ProductDto() { ProductId = 2,  Description = "Sweet Grapes",  Name = "Grapes" }
                }
            };

            _productsQueryService.Setup(c => c.GetInStockProducts()).Returns(_productsToReturn);
            _response = (FormattedContentResult<ResultDto<List<ProductDto>>>)_productsController.Get();
        }

        [TestMethod]
        public void calls_service_to_get_in_stock_products()
        {
            _productsQueryService.Verify(a => a.GetInStockProducts(), Times.Once);
        }

        [TestMethod]
        public void returns_ok_status_code()
        {
            Assert.AreEqual(HttpStatusCode.OK, _response.StatusCode);
        }

        [TestMethod]
        public void returns_correct_result()
        {
            Assert.AreEqual(_productsToReturn.HasErrors, _response.Content.HasErrors);
            Assert.AreEqual(_productsToReturn.Errors.Count, _response.Content.Errors.Count);
            Assert.IsNotNull(_productsToReturn.Result);

            foreach(var product in _productsToReturn.Result)
            {
                var returnedProduct = _response.Content.Result.SingleOrDefault(c => c.ProductId == product.ProductId
                                                                                 && c.Name == product.Name
                                                                                 && c.Description == product.Description);
                Assert.IsNotNull(returnedProduct);
            }
        }
    }
}
