﻿using System.Net;
using System.Web.Http;
using System.Net.Http.Formatting;

using Shopping.Business.Dtos;

namespace ShoppingApi.Controllers.Api
{
    public abstract class BaseApiController : ApiController
    {
        public IHttpActionResult Json<TResult>(ResultDto<TResult> result) where TResult : class
        {
            var status = HttpStatusCode.OK;
            if (result.HasErrors)
            {
                status = HttpStatusCode.BadRequest;
            }

            return Content(status, result, new JsonMediaTypeFormatter());
        }
    }
}
