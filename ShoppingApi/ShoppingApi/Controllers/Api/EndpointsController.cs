﻿using System.Linq;
using System.Web.Http;
using System.Reflection;
using System.Collections.Generic;

using ShoppingApi.Constants;

namespace ShoppingApi.Controllers.Api
{
    [RoutePrefix("")]
    public class EndpointsController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            var result = new
            {
                Urls = GetApiUrls()
            };

            return Json(result);
        }

        protected object GetApiUrls()
        {
            var routesList = new Dictionary<string, object>();
            var apiRoutes = ((TypeInfo)typeof(ApiRoutes)).DeclaredMembers;

            string prefixPropertyName = "Prefix";
            foreach (var apiRoute in apiRoutes)
            {
                var routeEndPoints = ((TypeInfo)apiRoute).DeclaredFields;
                var endpoints = new Dictionary<string, string>();

                object routePrefix = routeEndPoints.SingleOrDefault(c => c.Name == prefixPropertyName).GetValue(null).ToString();
                foreach (var routeEndPoint in routeEndPoints.Where(c => c.Name != prefixPropertyName))
                {
                    object endPointValue = routeEndPoint.GetValue(null);
                    endpoints.Add(routeEndPoint.Name, $"/{routePrefix}{getEndPointValue(endPointValue)}");
                }

                routesList.Add(apiRoute.Name, endpoints);
            }

            return routesList;
        }

        private string getEndPointValue(object endPoint)
        {
            string endPointValue = endPoint.ToString();
            if (!string.IsNullOrEmpty(endPointValue))
            {
                endPointValue = $"/{endPointValue}";
            }
            return endPointValue;
        }
    }
}
