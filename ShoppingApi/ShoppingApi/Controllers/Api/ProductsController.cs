﻿using System.Web.Http;

using ShoppingApi.Constants;

using Shopping.Business.Services;

namespace ShoppingApi.Controllers.Api
{
    [RoutePrefix(ApiRoutes.Product.Prefix)]
    public class ProductsController : BaseApiController
    {
        private IProductsQueryService _productsQueryService;
        public ProductsController(IProductsQueryService productsQueryService)
        {
            _productsQueryService = productsQueryService;
        }

        [HttpGet]
        [Route(ApiRoutes.Product.Get)]
        public IHttpActionResult Get()
        {
            var products = _productsQueryService.GetInStockProducts();
            return Json(products);
        }      
    }
}
