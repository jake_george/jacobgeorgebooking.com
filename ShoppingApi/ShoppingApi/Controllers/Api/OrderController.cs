﻿using System.Web.Http;

using ShoppingApi.Constants;

using Shopping.Business.Commands;
using Shopping.Business.Services;

namespace ShoppingApi.Controllers.Api
{
    [RoutePrefix(ApiRoutes.Order.Prefix)]
    public class OrderController : BaseApiController
    {
        private IOrderQueryService _orderQueryService;
        private IClearOrderService _clearOrderService;
        private ICreateOrderService _createOrderService;
        private IUpdateOrderItemService _updateOrderItemService;
        public OrderController(IOrderQueryService orderQueryService,
                               IClearOrderService clearOrderService,
                               ICreateOrderService createOrderService,
                               IUpdateOrderItemService updateOrderItemService)
        {
            _clearOrderService = clearOrderService;
            _orderQueryService = orderQueryService;
            _createOrderService = createOrderService;
            _updateOrderItemService = updateOrderItemService;
        }

        [HttpGet]
        [Route(ApiRoutes.Order.Get)]
        public IHttpActionResult Get(int orderId)
        {
            var existingOrder = _orderQueryService.GetOrder(orderId);
            return Json(existingOrder);
        }

        [HttpPost]
        [Route(ApiRoutes.Order.Create)]
        public IHttpActionResult Create()
        {
            var newOrder = _createOrderService.CreateOrder();
            return Json(newOrder);
        }

        [HttpPut]
        [Route(ApiRoutes.Order.Clear)]
        public IHttpActionResult clear(int orderId)
        {
            var updatedOrder = _clearOrderService.ClearItems(orderId);
            return Json(updatedOrder);
        }

        [HttpPut]
        [Route(ApiRoutes.Order.Update)]
        public IHttpActionResult update(UpdateOrderItemCommand updateCommand, int orderId)
        {
            updateCommand.OrderId = orderId;
            var updatedOrder = _updateOrderItemService.UpdateItemQuantity(updateCommand);
            return Json(updatedOrder);
        }
    }
}
