﻿namespace ShoppingApi.Constants
{
    public static class ApiRoutes
    {
        public static class Order
        {
            public const string Prefix = "api/order";
            public const string Create = "create";
            public const string Get = "{orderId}";
            public const string Clear = "{orderId}/items/clear";
            public const string Update = "{orderId}/items/update";
        }

        public static class Product
        {
            public const string Prefix = "api/products";
            public const string Get = "";
        }
    }
}