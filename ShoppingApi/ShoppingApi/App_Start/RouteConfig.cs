﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ShoppingApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");         
        }
    }
}
