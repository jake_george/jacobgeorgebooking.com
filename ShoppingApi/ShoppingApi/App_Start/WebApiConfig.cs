﻿using System.Web.Http;
using System.Web.Http.Cors;

using ShoppingApi.IoC;

namespace ShoppingApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var unityContainer = UnityConfig.RegisterComponents();
            config.DependencyResolver = new UnityResolver(unityContainer);

            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    } 
}
