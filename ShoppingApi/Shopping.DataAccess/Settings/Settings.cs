﻿using System.Configuration;

namespace Shopping.DataAccess.Settings
{
    public interface ISettings
    {        
        string ConnectionString { get; }
    }

    public class Settings : ISettings
    {
        public string ConnectionString { get; private set; }

        public Settings()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        }
    }
}
