﻿using System;
using System.Data;
using System.Data.Entity;

using Shopping.Entities;
using Shopping.DataAccess.Settings;

namespace Shopping.DataAccess.UnitOfWork
{
    public class EntityFrameworkUnitOfWork : IUnitOfWork
    {
        private DbContext _context = null;
        private ISettings _settings;
        private static readonly object lockObject = new object();
        public EntityFrameworkUnitOfWork(ISettings settings)
        {
            _settings = settings;
        }

        public DbContext Context
        {
            get
            {
                lock (lockObject)
                {
                    if (_context == null)
                    {
                        Database.SetInitializer<DbContext>(null);
                        var dbCompliedModel = DbCompliedModelFactory.Get(typeof(Order));
                        _context = new DbContext(_settings.ConnectionString, dbCompliedModel);
                    }

                    return _context;
                }
            }
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }     

        public ITransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return new EntityFrameworkTransaction(this, isolationLevel);
        }
    }
}
