﻿using System;

namespace Shopping.DataAccess.UnitOfWork
{
    public interface ITransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}
