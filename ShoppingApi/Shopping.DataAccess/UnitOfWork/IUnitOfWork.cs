﻿using System;
using System.Data;

namespace Shopping.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        void SaveChanges();
        ITransaction BeginTransaction(IsolationLevel isolationLevel);
    }
}
