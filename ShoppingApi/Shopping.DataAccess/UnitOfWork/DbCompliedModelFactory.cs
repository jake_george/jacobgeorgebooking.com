﻿using System;
using System.Linq;
using System.Reflection;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace Shopping.DataAccess.UnitOfWork
{
    public static class DbCompliedModelFactory
    {
        private static DbCompiledModel _dbCompliedModel = null;
        private static readonly object lockObject = new object();
        public static DbCompiledModel Get(Type entityExampleType)
        {
            lock (lockObject)
            {
                if (_dbCompliedModel == null)
                {

                    var modelBuilder = new DbModelBuilder();

                    var addMethod = typeof(ConfigurationRegistrar).GetMethods()
                                                                  .Single(m => m.Name == "Add"
                                                                            && m.GetGenericArguments()
                                                                            .Any(a => a.Name == "TEntityType"));

                    var currentAssembly = Assembly.GetAssembly(typeof(DbCompliedModelFactory));

                    var configurationsToRegister = currentAssembly.GetTypes().Where(t => t.BaseType != null
                                                                                      && t.BaseType.IsGenericType
                                                                                      && t.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));

                    foreach (var configurationToRegister in configurationsToRegister)
                    {
                        if (configurationToRegister.BaseType != null)
                        {
                            var entityType = configurationToRegister.BaseType.GetGenericArguments().Single();

                            var entityConfig = currentAssembly.CreateInstance(configurationToRegister.FullName);
                            addMethod.MakeGenericMethod(entityType).Invoke(modelBuilder.Configurations, new[] { entityConfig });
                        }
                    }

                    var entityMethod = modelBuilder.GetType().GetMethod("Entity");

                    var entitiesAssembly = Assembly.GetAssembly(entityExampleType);

                    var entitiesToRegister = entitiesAssembly.GetTypes().Where(t => !t.IsNested && !t.IsGenericType);
                    foreach (var entityToRegister in entitiesToRegister)
                    {
                        entityMethod.MakeGenericMethod(entityToRegister).Invoke(modelBuilder, new object[] { });
                    }

                    _dbCompliedModel = modelBuilder.Build(new DbProviderInfo("System.Data.SqlClient", "2012")).Compile();
                }

                return _dbCompliedModel;
            }
        }
    }
}
