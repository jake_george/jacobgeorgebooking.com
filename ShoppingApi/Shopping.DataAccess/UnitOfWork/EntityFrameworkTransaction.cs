﻿using System.Data;
using System.Data.Entity;

namespace Shopping.DataAccess.UnitOfWork
{
    public class EntityFrameworkTransaction : ITransaction
    {
        private readonly DbContextTransaction _transaction;
        public EntityFrameworkTransaction(EntityFrameworkUnitOfWork unitOfWork, IsolationLevel isolationLevel)
        {
            _transaction = unitOfWork.Context.Database.BeginTransaction(isolationLevel);
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public void Dispose()
        {
            _transaction.Dispose();
        }
    }
}
