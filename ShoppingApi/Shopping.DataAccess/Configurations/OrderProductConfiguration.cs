﻿using System.Data.Entity.ModelConfiguration;

using Shopping.Entities;

namespace Shopping.DataAccess.Configurations
{
    public class OrderProductConfiguration : EntityTypeConfiguration<OrderProduct>
    {
        public OrderProductConfiguration()
        {
            ToTable("OrderProducts");

            HasKey(q => new { q.OrderId, q.ProductId });
           
            Property(x => x.Quantity).HasColumnName("Quantity");
        }
    }
}
