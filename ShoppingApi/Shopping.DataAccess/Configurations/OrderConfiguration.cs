﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

using Shopping.Entities;

namespace Shopping.DataAccess.Configurations
{
    public class OrderConfiguration : EntityTypeConfiguration<Order> 
    {
        public OrderConfiguration()
        {            
            ToTable("Orders");

            HasKey(c => c.Id);

            Property(x => x.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.DateCreated).HasColumnName("DateCreated");
        }
    }
}
