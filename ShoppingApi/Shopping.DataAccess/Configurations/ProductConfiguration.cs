﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using Shopping.Entities;

namespace Shopping.DataAccess.Configurations
{
    public class ProductConfiguration : EntityTypeConfiguration<Product> 
    {
        public ProductConfiguration()
        {
            ToTable("Products");

            HasKey(c => c.Id);

            Property(x => x.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Name).HasColumnName("Name").HasMaxLength(70);

            Property(x => x.Stock).HasColumnName("Stock");

            Property(x => x.Description).HasColumnName("Description").HasMaxLength(500);
        }
    }
}
