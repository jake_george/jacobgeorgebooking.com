﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Collections.Generic;
using Shopping.DataAccess.UnitOfWork;

namespace Shopping.DataAccess.Repositories
{
    public abstract class BaseEntityFrameworkRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private EntityFrameworkUnitOfWork _unitOfWork;
        public BaseEntityFrameworkRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (EntityFrameworkUnitOfWork)unitOfWork;
        }

        private IDbSet<TEntity> Db()
        {
            return _unitOfWork.Context.Set<TEntity>();
        }

        protected IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> sequence = Db().Where(predicate);
            return sequence.ToList();
        }

        protected TEntity GetSingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> sequence = Db();
            return sequence.SingleOrDefault(predicate);
        }

        public TEntity Add(TEntity entity)
        {
            return Db().Add(entity);
        }

        public void Remove(TEntity entity)
        {
            Db().Remove(entity);
        }
    }
}
