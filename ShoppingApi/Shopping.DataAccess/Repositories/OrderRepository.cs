﻿using Shopping.Entities;

using Shopping.DataAccess.UnitOfWork;

namespace Shopping.DataAccess.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        Order GetById(int id);
    }

    public class OrderRepository : BaseEntityFrameworkRepository<Order>, IOrderRepository
    {
        public OrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {  }

        public Order GetById(int id)
        {
            return this.GetSingleOrDefault(c => c.Id == id);
        }
    }
}
