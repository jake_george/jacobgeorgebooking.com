﻿using System.Linq;
using System.Collections.Generic;

using Shopping.Entities;
using Shopping.DataAccess.UnitOfWork;

namespace Shopping.DataAccess.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetById(int id);
        List<Product> GetAllInStockProducts();
    }

    public class ProductRepository : BaseEntityFrameworkRepository<Product>, IProductRepository
    {
        public ProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {  }

        public Product GetById(int id)
        {
            return this.GetSingleOrDefault(c => c.Id == id);
        }

        public List<Product> GetAllInStockProducts()
        {
            return this.GetList(c => c.Stock > 0).ToList();
        }
    }
}
