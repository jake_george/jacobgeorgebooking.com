import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';
import { ShoppingFrameWorkConfigProvider } from './providers/config-provider/config-provider';

export interface IShoppingFrameworkConfig {
  url: string;
  apiKey: string;
}

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [],
  exports: []
})
export class ShoppingFrameworkModule {
  static forRoot(config: IShoppingFrameworkConfig): ModuleWithProviders {
    return {
      ngModule: ShoppingFrameworkModule,
      providers: [
        {
          provide: ShoppingFrameWorkConfigProvider,
          useValue: config
        },
        CookieService
      ]
    }
  }
}
