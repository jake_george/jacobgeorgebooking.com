export class BasketItem {
  public ProductId: number;
  public Name: string;
  public Quantity: number;
  public Description: string;
}
