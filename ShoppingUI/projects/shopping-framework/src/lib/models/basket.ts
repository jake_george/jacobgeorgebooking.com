import { BasketItem } from './basket-item'

export class Basket {
  public OrderId: number;
  public Items: BasketItem[]
}
