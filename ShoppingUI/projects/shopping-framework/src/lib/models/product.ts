export class Product {
  public ProductId: number;
  public Name: string;
  public SelectedQuantity: number;
  public Description: string;
}
