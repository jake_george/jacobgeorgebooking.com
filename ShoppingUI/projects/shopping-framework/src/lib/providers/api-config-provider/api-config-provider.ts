import { Injectable, Inject } from '@angular/core';

import { ShoppingFrameWorkConfigProvider } from '../../providers/config-provider/config-provider';

import { HttpApiProvider } from '../../providers/http-api-provider/http-api-provider';

@Injectable({ providedIn: 'root' })
export class ApiConfigProvider {
  constructor(@Inject(ShoppingFrameWorkConfigProvider) private config,
    private httpApiProvider: HttpApiProvider) { }

  private apiConfig: any = null;

  public getConfig(): Promise<any> {
    var promise: Promise<any> = new Promise<any>((resolve, reject) => {
      if (this.apiConfig !== null) {
        resolve(this.apiConfig);
      }
      else {
        this.httpApiProvider.get(this.config.url).then((result) => {
          this.apiConfig = result;
          resolve(result);
        }).catch((result) => {
          reject(result);
        });
      }
    });
    return promise;   
  }

}
