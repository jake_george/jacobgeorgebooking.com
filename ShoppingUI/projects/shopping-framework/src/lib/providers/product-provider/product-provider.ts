import { Injectable, Inject } from '@angular/core';

import { HttpApiProvider } from '../../providers/http-api-provider/http-api-provider';
import { ApiConfigProvider } from '../../providers/api-config-provider/api-config-provider';
import { ShoppingFrameWorkConfigProvider } from '../../providers/config-provider/config-provider';

@Injectable({ providedIn: 'root' })
export class ProductProvider {
  constructor(@Inject(ShoppingFrameWorkConfigProvider) private config,
    private httpApiProvider: HttpApiProvider,
    private apiConfigProvider: ApiConfigProvider) { }

  public getAllProducts(): Promise<any> {
    var promise: Promise<any> = new Promise<any>((resolve, reject) => {
      this.apiConfigProvider.getConfig().then((result) => {
        this.httpApiProvider.get(this.config.url + result.Urls.Product.Get).then((productsResult) => {
          resolve(productsResult);
        });
      });
    });
    return promise;
  }

}
