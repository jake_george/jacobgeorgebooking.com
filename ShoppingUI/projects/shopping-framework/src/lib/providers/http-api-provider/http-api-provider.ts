import { Injectable } from '@angular/core';
import {
    Http,
    Response,
    Headers,
    RequestOptionsArgs,
    RequestOptions,
    RequestMethod,
    URLSearchParams
} from '@angular/http';

@Injectable({ providedIn: 'root' })
export class HttpApiProvider {

  constructor(private http: Http) { }

  public get(url: string, options?: RequestOptionsArgs): Promise<any> {
    let options1 = new RequestOptions({ method: RequestMethod.Get });
    options1 = options1.merge(options);
    return this.sendRequest(url, options1);
  }

  public post(url: string, body?: any, options?: RequestOptionsArgs): Promise<any> {
    let options1 = new RequestOptions({ method: RequestMethod.Post });
    options1 = options1.merge(options);
    options1.body = body;
    return this.sendRequest(url, options1);
  }

  public patch(url: string, body?: any, options?: RequestOptionsArgs): Promise<any> {
    let options1 = new RequestOptions({ method: RequestMethod.Patch });
    options1.body = body;
    options1 = options1.merge(options);
    return this.sendRequest(url, options1);
  }

  public put(url: string, body?: any, options?: RequestOptionsArgs): Promise<any> {
    let options1 = new RequestOptions({ method: RequestMethod.Put });
    options1 = options1.merge(options);
    options1.body = body;
    return this.sendRequest(url, options1);
  }


  sendRequest(url: string, options: RequestOptionsArgs): Promise<any> {
    const options1 = new RequestOptions();
    options1.method = options.method;

    if (options.search != null) {
      options1.search = new URLSearchParams(options.search.toString()).clone();
    }

    if (options.headers != null) {
      options1.headers = new Headers(options.headers.toJSON());
    }

    if (options.body) {
      options1.body = options.body;
    }
    options1.withCredentials = options.withCredentials;

    let promise: Promise<any> = new Promise((resolve, reject) => {
      this.http
        .request(url, options1)
        .toPromise()
        .then((res) => {
          if (res.status !== 200) {
            throw new Error('Bad response status: ' + res.status);
          }
          else {
            let body = res.json();
            resolve(body || {});
          }
        })
        .catch((error: any) => {
          reject(JSON.parse(error._body));
        });
    });

    return promise;
  }
}
