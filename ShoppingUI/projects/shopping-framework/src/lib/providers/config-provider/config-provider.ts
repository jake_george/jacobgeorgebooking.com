import { InjectionToken } from '@angular/core';

import { IShoppingFrameworkConfig } from '../../shopping-framework.module';

export const ShoppingFrameWorkConfigProvider = new InjectionToken<IShoppingFrameworkConfig>("ShoppingFrameworkConfig");
