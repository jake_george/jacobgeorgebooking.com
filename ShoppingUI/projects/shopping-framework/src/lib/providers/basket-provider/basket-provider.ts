import { Injectable, Inject } from '@angular/core';

import { Basket } from '../../models/basket';
import { UpdateBasketItem } from '../../models/update-basket-item';
import { AddProductRequest } from '../../models/add-product-request';

import { ShoppingFrameWorkConfigProvider } from '../../providers/config-provider/config-provider';

import { CookieService } from 'ngx-cookie-service';
import { HttpApiProvider } from '../../providers/http-api-provider/http-api-provider';
import { ApiConfigProvider } from '../../providers/api-config-provider/api-config-provider';

@Injectable({ providedIn: 'root' })
export class BasketProvider {
  constructor(@Inject(ShoppingFrameWorkConfigProvider) private config,
    private apiConfigProvider: ApiConfigProvider,
    private httpApiProvider: HttpApiProvider,
    private cookieService: CookieService) { }

  private setBasket(basket: Basket) {
    this.cookieService.set("currentBasket", JSON.stringify(basket));
  }

  private getBasket(): any {
    var basketObject = null;
    var currentBasket = this.cookieService.get("currentBasket");
    if (currentBasket !== "") {
      basketObject = JSON.parse(currentBasket);
    }
    return basketObject;
  }

  public createBasket(): Promise<any> {
    var promise: Promise<any> = new Promise<any>((resolve, reject) => {
      this.apiConfigProvider.getConfig().then((result) => {
        this.httpApiProvider.post(this.config.url + result.Urls.Order.Create).then((result) => {
          this.setBasket(result.Result);
          resolve(result);
        }).catch((result) => {
          reject(result);
        });
      });     
    });
    return promise;
  }

  public getCurrentBasket(): Promise<any> {
    var promise: Promise<any> = new Promise<any>((resolve, reject) => {
      var currentBasket = this.getBasket();
      if (currentBasket === null) {
        this.createBasket().then((result) => {
          this.setBasket(result.Result);
          resolve(result);
        }).catch((result) => {
          reject(result);
        });
      }
      else {
        this.apiConfigProvider.getConfig().then((result) => {
          this.httpApiProvider.get(this.config.url + result.Urls.Order.Get.replace("{orderId}", currentBasket.OrderId)).then((result) => {
            this.setBasket(result.Result);
            resolve(result);
          }).catch((result) => {
            reject(result);
          });
        });        
      }     
    });
    return promise;   
  }

  public addBasketItem(addProductRequest: AddProductRequest): Promise<any> {
    var promise: Promise<any> = new Promise<any>((resolve, reject) => {
      this.getCurrentBasket().then((basketResult) => {
        var sameProductInCurrentBasket = basketResult.Result.Items.filter(c => c.ProductId === addProductRequest.ProductId);
        if (sameProductInCurrentBasket.length > 0) {
          addProductRequest.Quantity = addProductRequest.Quantity + sameProductInCurrentBasket[0].Quantity;
        }
        this.apiConfigProvider.getConfig().then((result) => {
          this.httpApiProvider.put(this.config.url + result.Urls.Order.Update.replace("{orderId}", basketResult.Result.OrderId), addProductRequest).then((result) => {
            this.setBasket(result.Result);
            resolve(result);
          }).catch((result) => {
            reject(result);
          });
        });        
      });
    });
    return promise;
  }

  public updateBasketItem(updateBasketItem: UpdateBasketItem): Promise<any> {
    var promise: Promise<any> = new Promise<any>((resolve, reject) => {
      this.getCurrentBasket().then((basketResult) => {
        this.apiConfigProvider.getConfig().then((result) => {
          this.httpApiProvider.put(this.config.url + result.Urls.Order.Update.replace("{orderId}", basketResult.Result.OrderId), updateBasketItem).then((result) => {
            this.setBasket(result.Result);
            resolve(result);
          }).catch((result) => {
            reject(result);
          });
        });       
      });
    });
    return promise;
  }

  public clearBasket(): Promise<any> {
    var promise: Promise<any> = new Promise<any>((resolve, reject) => {
      this.getCurrentBasket().then((basketResult) => {
        this.apiConfigProvider.getConfig().then((result) => {
          this.httpApiProvider.put(this.config.url + result.Urls.Order.Clear.replace("{orderId}", basketResult.Result.OrderId)).then((result) => {
            this.setBasket(result.Result);
            resolve(result);
          }).catch((result) => {
            reject(result);
          });
        });      
      });
    });
    return promise;   
  }

}
