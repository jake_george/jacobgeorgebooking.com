import { Injectable } from '@angular/core';

import { ToastrManager } from 'ng6-toastr-notifications';

@Injectable()
export class NotificationProvider {
  private toastsManager: ToastrManager = null;
  private messageConfig: any = {
    toastTimeout: 3000,
    animate: "slideFromTop",
    showCloseButton: true,
    newestOnTop: true,
  };
  constructor(private toastMgr: ToastrManager) {
    if (this.toastsManager === null) {
      this.toastsManager = toastMgr;
    }
  }

  public info(message: string): void {
    this.toastsManager.infoToastr(message, "Info", this.messageConfig);
  }

  public success(message: string): void {
    this.toastsManager.successToastr(message, "Success", this.messageConfig);
  }

  public warning(message: string): void {
    this.toastsManager.warningToastr(message, "Warning", this.messageConfig);
  }

  public error(message: string): void {
    this.toastsManager.errorToastr(message, "Error", this.messageConfig);
  }
}
