import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Basket } from 'ShoppingFramework';
import { BasketProvider } from 'ShoppingFramework';

@Injectable()
export class ResolveBasketProvider implements Resolve<any> {
  constructor(private basketProvider: BasketProvider) { }
    
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.basketProvider.getCurrentBasket();
  }
}
