import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Product } from 'ShoppingFramework';
import { ProductProvider } from 'ShoppingFramework';

@Injectable()
export class ResolveProductsProvider implements Resolve<any> {
  constructor(private productProvider: ProductProvider) { }
    
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.productProvider.getAllProducts(); 
  }
}
