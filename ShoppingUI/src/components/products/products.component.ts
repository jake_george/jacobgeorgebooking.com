import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Product } from 'ShoppingFramework';
import { AddProductRequest } from 'ShoppingFramework';

import { BasketProvider } from 'ShoppingFramework';
import { NotificationProvider } from '../../providers/notification-provider/notification-provider';

@Component({
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {
  public products: Product[];
  constructor(
    private activatedRoute: ActivatedRoute,
    private basketProvider: BasketProvider,
    private notificationProvider: NotificationProvider) { }

  ngOnInit() {
    this.loadProductData();
  }

  private loadProductData() {
    this.products = this.activatedRoute.snapshot.data.productsData.Result;
  }

  public addProductToBasket(productToAdd: Product): void {
    var addProductRequest: AddProductRequest = {
      ProductId: productToAdd.ProductId,
      Quantity: productToAdd.SelectedQuantity
    };    

    this.basketProvider.addBasketItem(addProductRequest).then(() => {
      this.notificationProvider.success("We have added the required " + productToAdd.Name + " to the basket");
    }).catch((exception) => {
      this.notificationProvider.error(exception.Errors[0]);
    });
  }
}
