import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Product } from 'ShoppingFramework';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html'
})
export class ProductCardComponent implements OnInit {
  private addNos: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  @Input() product: Product;
  @Input() defaultQuantity: boolean = true;
  @Input() buttonText: string = "Add";
  @Output() buyProductClick = new EventEmitter<Product>();

  constructor() { }

  ngOnInit() {
    if (this.defaultQuantity === true) {
      this.initSelectedQuantity();
    }  
  }

  private initSelectedQuantity() {
    this.product.SelectedQuantity = 1;
  }

  public addItemToCart(productToAdd: Product): void {  
    this.buyProductClick.emit(productToAdd);  
  }
}
