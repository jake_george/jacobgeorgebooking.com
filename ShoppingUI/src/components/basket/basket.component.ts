import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

import { Basket } from 'ShoppingFramework';
import { Product } from 'ShoppingFramework';
import { UpdateBasketItem } from 'ShoppingFramework';

import { BasketProvider } from 'ShoppingFramework';
import { NotificationProvider } from '../../providers/notification-provider/notification-provider';

@Component({
  templateUrl: './basket.component.html'
})
export class BasketComponent implements OnInit {
  public products: Product[];
  constructor(private activatedRoute: ActivatedRoute,
    private basketProvider: BasketProvider,
    private notificationProvider: NotificationProvider) { }

  ngOnInit() {
    var basket: Basket = this.activatedRoute.snapshot.data.basketData.Result;
    this.loadBasketData(basket);
  }

  private loadBasketData(basket: Basket) {
    this.products = [];
    for (var i = 0; i < basket.Items.length; i++) {
      var product: Product = {
        ProductId: basket.Items[i].ProductId,
        Name: basket.Items[i].Name,
        SelectedQuantity: basket.Items[i].Quantity,
        Description: basket.Items[i].Description
      };
      this.products.push(product);
    }
  }

  public basketIsEmpty(): boolean {
    return this.products.length === 0;
  }

  public updateBasketItem(productToAdd: Product): void {
    var updateBasketItem: UpdateBasketItem = {
      ProductId: productToAdd.ProductId,
      Quantity: productToAdd.SelectedQuantity
    };

    this.basketProvider.updateBasketItem(updateBasketItem).then(() => {
      this.notificationProvider.success("We have updated the quantity of " + productToAdd.Name + " in the basket to " + productToAdd.SelectedQuantity);
    }).catch((exception) => {
      this.notificationProvider.error(exception.Errors[0]);
    });
  }

  public clearBasketClick(): void {
    this.basketProvider.clearBasket().then((result) => {
      var basket: Basket = result.Result;
      this.loadBasketData(basket);
      this.notificationProvider.success("We have cleared your basket");
    }).catch((exception) => {
      this.notificationProvider.error(exception.Errors[0]);
    });
  }
}
