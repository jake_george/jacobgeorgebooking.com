import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ng6-toastr-notifications';

import { ShoppingFrameworkModule, IShoppingFrameworkConfig } from 'ShoppingFramework';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { BasketComponent } from '../components/basket/basket.component';
import { ProductsComponent } from '../components/products/products.component';
import { ProductCardComponent } from '../components/product-card/product-card.component';

import { ResolveBasketProvider } from '../providers/resolves/resolve-basket.provider';
import { ResolveProductsProvider } from '../providers/resolves/resolve-products.provider';
import { NotificationProvider } from '../providers/notification-provider/notification-provider';

const shoppingFrameWorkConfig: IShoppingFrameworkConfig = {
  url: "http://localhost:22772/",
  apiKey: "AIzaSyAphwCKmji5MQCKhqeEH7V-sM53Zu-ZJxc"
};

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    BasketComponent,
    ProductCardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    ShoppingFrameworkModule.forRoot(shoppingFrameWorkConfig),
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    ResolveBasketProvider,
    ResolveProductsProvider,
    NotificationProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
