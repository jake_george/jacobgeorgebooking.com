import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasketComponent } from '../components/basket/basket.component';
import { ProductsComponent } from '../components/products/products.component';
import { ResolveBasketProvider } from '../providers/resolves/resolve-basket.provider';
import { ResolveProductsProvider } from '../providers/resolves/resolve-products.provider';

const routes: Routes = [
  {
    path: 'basket',
    component: BasketComponent,
    resolve: { basketData: ResolveBasketProvider }
  },
  {
    path: 'products',
    component: ProductsComponent,
    resolve: { productsData: ResolveProductsProvider }
  },
  {
    path: '**',
    redirectTo: '/products'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
